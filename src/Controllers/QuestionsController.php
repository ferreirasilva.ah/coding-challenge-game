<?php


namespace Ucc\Controllers;


use Ucc\Services\QuestionService;
use Ucc\Session;
use Ucc\Http\JsonResponseTrait;

class QuestionsController extends Controller
{
    private $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        //print_r($_REQUEST);exit;
        $name = $_REQUEST['name'] ?? null;
        if (empty($name)) {
            return JsonResponseTrait::json('You must provide a name', 400);
        }
        
        Session::set('name', $name);
        Session::set('questionCount', 1);
        //TODO Get first question for user
        $questions = $this->getQuestions();

        // Taking a random question for the user
        $rand = rand(1,count($questions));
        Session::set('questionLast', $rand);
        $question = $questions[$rand-1];

        return JsonResponseTrait::json(['question' => $question->question, 'id' => $question->id], 201);
    }

    public function answerQuestion(int $id): bool 
    {
        if ( Session::get('name') === null ) {
            return JsonResponseTrait::json('You must first begin a game', 400);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return JsonResponseTrait::json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $_REQUEST['answer'] ?? null;
        if (empty($answer)) {
            return JsonResponseTrait::json('You must provide an answer', 400);
        }

        //TODO: Check answer and increment user's points. Reply with a proper message
        $message = '';
        $question = null;
        $questionId = null;
        $questions = $this->getQuestions();
        
        if($answer == $questions[$id-1]) {
            $points = Session::get('points');
            Session::set('points',$points+1);
            $message = 'Good job!';
        } else {
            $message = 'Wrong answer!';
        }

        $rand = rand(1,count($questions));
        Session::set('questionLast', $rand);
        $question = $questions[$rand-1];

        return JsonResponseTrait::json(['message' => $message, 'question' => $question->question, 'id' => $question->id]);
    }

    public function getQuestions(): array
    {
        return json_decode(file_get_contents('questions.json'));
    }

}